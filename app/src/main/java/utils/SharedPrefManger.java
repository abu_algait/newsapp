package utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;
import com.abualgait.newsapp.LoginActicity;

public class SharedPrefManger {
    Activity activity;
    Context context;

    public SharedPreferences sharedPreferences;
    private static final String FILE_NAME = "myFile";

    //  Constructor : Set Context
    public SharedPrefManger(Activity context) {
        this.activity = context;
        sharedPreferences = activity.getSharedPreferences(FILE_NAME, 0);
    }

    public SharedPrefManger(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(FILE_NAME, 0);
    }

    // Set Value to Key
    public void setEditor(String key, String value) {
        sharedPreferences.edit().putString(key, value).apply();
    }

    // Set Key and Get Value
    public String getEditor(String key) {
        return sharedPreferences.getString(key, "");
    }

    public void logOut() {
        FirebaseAuth.getInstance().signOut();
        setEditor(ConstantValues.IS_LOGGED_KEY, "false");
        Intent intent = new Intent(activity, LoginActicity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

}
