package utils;

public class ConstantValues {
    public static String EMAIL_KEY = "email";
    public static String USER_KEY = "user";
    public static String PASSWORD_KEY = "pass";
    public static String NAME_KEY = "name";
    public static String IMAGE_KEY = "image";
    public static String IS_LOGGED_KEY = "islogged";
    public static String NOTIFICATION_COUNT = "notifi_count";
    public static int APPOINTEMENT_COUNT = 0;

}
