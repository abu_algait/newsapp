package models;

import com.google.gson.annotations.SerializedName;

public class Photo {

    @SerializedName("titleRetro")
    private String title;
    @SerializedName("imageRetro")
    private String image;

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
