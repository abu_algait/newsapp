package models;

public class Comment {
    public Comment(String commnet_text, String comment_time, String owner_email, String owner_image) {
        this.commnet_text = commnet_text;
        this.comment_time = comment_time;
        this.owner_email = owner_email;
        this.owner_image = owner_image;
    }

    public Comment() {
    }

    @Override
    public String toString() {
        return "Comment{" +
                "commnet_text='" + commnet_text + '\'' +
                ", comment_time='" + comment_time + '\'' +
                ", owner_email='" + owner_email + '\'' +
                ", owner_image='" + owner_image + '\'' +
                '}';
    }

    public String getCommnet_text() {
        return commnet_text;
    }

    public void setCommnet_text(String commnet_text) {
        this.commnet_text = commnet_text;
    }

    public String getComment_time() {
        return comment_time;
    }

    public void setComment_time(String comment_time) {
        this.comment_time = comment_time;
    }

    public String getOwner_email() {
        return owner_email;
    }

    public void setOwner_email(String owner_email) {
        this.owner_email = owner_email;
    }

    public String getOwner_image() {
        return owner_image;
    }

    public void setOwner_image(String owner_image) {
        this.owner_image = owner_image;
    }

    private String commnet_text;
    private String comment_time;
    private String owner_email;
    private String owner_image;
}
