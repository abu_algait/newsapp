package com.abualgait.newsapp;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static ApiInterfacesFunctions jsonPlaceHolderApi;


    public static ApiInterfacesFunctions getClient(String Base_url) {

        //Base_url = "https://api.jsonbin.io/";
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonPlaceHolderApi = retrofit.create(ApiInterfacesFunctions.class);
        return jsonPlaceHolderApi;
    }

}
