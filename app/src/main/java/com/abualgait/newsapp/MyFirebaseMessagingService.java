package com.abualgait.newsapp;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import models.NotificationRepo;
import utils.ConstantValues;
import utils.SharedPrefManger;

import static utils.ConstantValues.APPOINTEMENT_COUNT;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private static final String YOUR_DESIRED_CHANNEL_ID_STRING = "1";
    private static final CharSequence YOUR_DESIRED_CHANNEL_LABEL_STRING = "label_string";
    private static final String YOUR_DESIRED_CHANNEL_DESC_STRING = "desc_notif";
    SharedPrefManger sharedPrefManager;
    static OnNotificationListener onNotificationListener;
    models.Notification notification;

    public MyFirebaseMessagingService(OnNotificationListener onNotificationListener) {
        this.onNotificationListener = onNotificationListener;

    }

    public MyFirebaseMessagingService() {
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //todo add to notification list and add to room data base
        String body = remoteMessage.getNotification().getBody();
        String title = remoteMessage.getNotification().getTitle();
        notification = new models.Notification(title, body);

        NotificationRepo notificationRepo = new NotificationRepo(getApplication());
        notificationRepo.insert(notification);

        sharedPrefManager = new SharedPrefManger(this);
        APPOINTEMENT_COUNT += 1;
        if (onNotificationListener != null)
            this.onNotificationListener.updateNotification(APPOINTEMENT_COUNT);


        //sharedPrefManager.setEditor(ConstantValues.NOTIFICATION_COUNT, "0");
        //sharedPrefManager.setEditor(ConstantValues.NOTIFICATION_COUNT, String.valueOf(
        //Integer.parseInt(sharedPrefManager.getEditor(ConstantValues.NOTIFICATION_COUNT)) + 1));


        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        showNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

    }


    private void showNotification(String body, String title) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            Intent my_intent = null;
            if (sharedPrefManager.getEditor(ConstantValues.IS_LOGGED_KEY).equals("true")) {
                my_intent = new Intent(this, MainActivity.class);
                my_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                Log.d(TAG, "\"main activity\": " + "main activity");
            } else {
                my_intent = new Intent(this, LoginActicity.class);
                my_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.d(TAG, "\"login activity\": " + "login activity");
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, my_intent, PendingIntent.FLAG_UPDATE_CURRENT);


            NotificationChannel chan1 = new NotificationChannel(
                    YOUR_DESIRED_CHANNEL_ID_STRING,
                    YOUR_DESIRED_CHANNEL_LABEL_STRING,
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationManager.createNotificationChannel(chan1);

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(this, YOUR_DESIRED_CHANNEL_ID_STRING);
            builder.setSmallIcon(R.mipmap.ic_launchernews);
            builder.setContentIntent(pendingIntent);
            builder.setContentTitle(title).setContentText(body);

            notificationManager.notify(0, builder.build());
        } else {

            Intent my_intent = null;
            if (sharedPrefManager.getEditor(ConstantValues.IS_LOGGED_KEY).equals("true")) {
                my_intent = new Intent(this, MainActivity.class);
                my_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                my_intent = new Intent(this, LoginActicity.class);
                my_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            }
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, my_intent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setAutoCancel(true)
                    .setContentTitle(title).setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launchernews)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setContentIntent(pendingIntent);


            notificationManager.notify(0, builder.build());
        }
    }


}