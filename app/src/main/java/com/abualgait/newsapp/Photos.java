package com.abualgait.newsapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import adapters.CommonAdapter;
import models.Photo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Photos extends Fragment {


    private List<Photo> photosList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommonAdapter mAdapter;
    private ProgressBar my_progress;

    public Photos() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.videos_fragment, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.photos_recycler_view);
        my_progress = view.findViewById(R.id.my_progress);
        mAdapter = new CommonAdapter(photosList, getContext(), 1);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());


        preparePhotoData();
        return view;
    }

    private void preparePhotoData() {
        Call<List<Photo>> call = ApiClient.getClient("https://api.jsonbin.io/").getPhotos();

        call.enqueue(new Callback<List<Photo>>() {
            @Override
            public void onResponse(Call<List<Photo>> call, Response<List<Photo>> response) {

                if (!response.isSuccessful()) {

                    return;
                }
                my_progress.setVisibility(View.GONE);
                photosList = response.body();
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Photo>> call, Throwable t) {

            }


        });


    }

}
