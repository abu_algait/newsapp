package com.abualgait.newsapp;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class ViewPagerAdapter extends FragmentPagerAdapter {


    // override on getItem : return fragment based on position(position come from FragmentPagerAdapter)
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new TopNews();
        } else if (position == 1) {
            fragment = new Briefs();
        } else if (position == 2) {
            fragment = new Photos();
        } else if (position == 3) {
            fragment = new Videos();
        }
        return fragment;
    }

    /////////////////////////// Tabs ///////////////////////////
    Context context;

    public ViewPagerAdapter(FragmentManager fm, Context c) {
        super(fm);
        this.context = c;
    }

    // override on getCount : return number of tabs
    @Override
    public int getCount() {
        return tabTitles.length;
    }

    private String tabTitles[] = new String[]{"TOP NEWS", "BRIEFS", "PHOTOS", "VIDEOS"};
    private int tabImages[] = new int[]{R.drawable.ic_trending_up_black_24dp,R.drawable.ic_short_text_black_24dp,R.drawable.ic_insert_photo_black_24dp,R.drawable.ic_slow_motion_video_black_24dp,};

    // fill custom_Tap depend on the Tap number
    public View getTabView(int position) {
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) v.findViewById(R.id.tab_name);
        tv.setText(tabTitles[position]);
        ImageView im = (ImageView) v.findViewById(R.id.tab_image);
        im.setImageResource(tabImages[position]);
        return v;
    }

}