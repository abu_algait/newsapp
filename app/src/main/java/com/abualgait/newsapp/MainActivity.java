package com.abualgait.newsapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import models.Upload;
import utils.ConstantValues;
import utils.SharedPrefManger;

import static utils.ConstantValues.APPOINTEMENT_COUNT;

public class MainActivity extends AppCompatActivity implements OnNotificationListener {

    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    DatabaseReference myRef;
    FirebaseDatabase database;
    SharedPrefManger sharedPrefManager;
    SharedPrefManger sharedPrefManager2;
    CircleImageView imageView;
    FrameLayout image_frame;
    public static TextView app_count = null;
    String my_noyi_counts;
    Integer counter;
    MyFirebaseMessagingService myFirebaseMessagingService;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myFirebaseMessagingService = new MyFirebaseMessagingService(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("uploads");
        sharedPrefManager = new SharedPrefManger(this);
        sharedPrefManager2 = new SharedPrefManger(getApplicationContext());
        sharedPrefManager2.setEditor(ConstantValues.NOTIFICATION_COUNT, "0");
        my_noyi_counts = sharedPrefManager2.getEditor(ConstantValues.NOTIFICATION_COUNT);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(), getApplicationContext());
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.whiteColor));

        // fill Custom_Tab
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(viewPagerAdapter.getTabView(i));
        }

        sharedPrefManager.setEditor(ConstantValues.IMAGE_KEY, "http://www.clker.com/cliparts/A/Y/O/m/o/N/placeholder-hi.png");

    }

    private void read_profile_image() {
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String user_id = postSnapshot.getKey();
                    if (user_id.equals(sharedPrefManager.getEditor(ConstantValues.USER_KEY))) {

                        for (DataSnapshot profile : postSnapshot.getChildren()) {
                            Upload profile_image_obj = profile.getValue(Upload.class);
                            sharedPrefManager.setEditor(ConstantValues.IMAGE_KEY, profile_image_obj.getImageUrl());
                            Picasso.get().load(sharedPrefManager.getEditor(ConstantValues.IMAGE_KEY)).into(imageView);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("The read failed: " + error.getMessage());
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.news_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.profile);
        image_frame = (FrameLayout) item.getActionView();
        imageView = image_frame.findViewById(R.id.imageViewProfile);
        app_count = image_frame.findViewById(R.id.app_count);

        if (APPOINTEMENT_COUNT != 0) {
            app_count.setText(APPOINTEMENT_COUNT + "");
        }
//        } else {
//            app_count.setVisibility(View.GONE);
//        }


        Picasso.get().load(sharedPrefManager.getEditor(ConstantValues.IMAGE_KEY)).into(imageView);
        read_profile_image();
        image_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, Profile.class);
                startActivity(i);
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final SharedPrefManger sharedPrefManager = new SharedPrefManger(this);

        switch (id) {
            case R.id.search:
                Toast.makeText(getApplicationContext(), "search Selected", Toast.LENGTH_LONG).show();
                return true;
            case R.id.settings:
                Toast.makeText(getApplicationContext(), "settings Selected", Toast.LENGTH_LONG).show();
                return true;

            case R.id.logout:
                sharedPrefManager.logOut();
                return true;

            case R.id.profile:
                Intent i = new Intent(MainActivity.this, Profile.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void updateNotification(Integer counter) {
        Log.d("TAG", "updateNotification: " + counter);

        if (counter != null) {
            this.counter = counter;
            if (app_count != null) {
                //app_count.setVisibility(View.VISIBLE);
                app_count.setText(counter + "");
            }

        }
    }

}
