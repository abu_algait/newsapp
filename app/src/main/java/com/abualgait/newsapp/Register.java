package com.abualgait.newsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import utils.ConstantValues;
import utils.SharedPrefManger;

public class Register extends AppCompatActivity {
    private static final String TAG = "Register";
    private FirebaseAuth mAuth;
    FirebaseUser currentUser;

    @BindView(R.id.username)
    EditText username;

    @BindView(R.id.email_register)
    EditText email_register_et;

    @BindView(R.id.password)
    EditText password_register_et;


    @BindView(R.id.register)
    Button register_btn;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        mAuth = FirebaseAuth.getInstance();
    }

    // make onClick properity on button register equal this function
    public void registerFn(View view) {
        switch (view.getId()) {
            case R.id.register:
                checkData();
                break;
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    public void checkData() {
        // Validation
        boolean cancel = false;
        View focusView = null;

        String my_username = username.getText().toString();
        String my_email = email_register_et.getText().toString();
        String my_password = password_register_et.getText().toString();

        // Check for a valid username, if the user entered one.
        if (TextUtils.isEmpty(my_username)) {
            username.setError(getString(R.string.error_field_required));
            focusView = username;
            cancel = true;
        }

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(my_password) && !isPasswordValid(my_password)) {
            password_register_et.setError(getString(R.string.error_invalid_password));
            focusView = password_register_et;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(my_email)) {
            email_register_et.setError(getString(R.string.error_field_required));
            focusView = email_register_et;
            cancel = true;
        } else if (!isEmailValid(my_email)) {
            email_register_et.setError(getString(R.string.error_invalid_email));
            focusView = email_register_et;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            register_via_firebase(my_email, my_password);
        }

    }


    private void register_via_firebase(String email, String password) {
        final SharedPrefManger sharedPrefManager = new SharedPrefManger(this);
        final SharedPrefManger sharedPrefManager2 = new SharedPrefManger(getApplicationContext());
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            String[] name = user.getEmail().split("@");
                            sharedPrefManager.setEditor(ConstantValues.IS_LOGGED_KEY, "true");
                            sharedPrefManager2.setEditor(ConstantValues.IS_LOGGED_KEY, "true");
                            sharedPrefManager.setEditor(ConstantValues.EMAIL_KEY, user.getEmail());
                            sharedPrefManager.setEditor(ConstantValues.NAME_KEY, name[0]);
                            sharedPrefManager.setEditor(ConstantValues.USER_KEY, user.getUid());
                            Intent intent = new Intent(Register.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "some thing went wrong", Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }
}
