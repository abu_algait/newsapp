package com.abualgait.newsapp;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import adapters.BriefAdapter;
import models.Article;
import models.EgyptNews;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Briefs extends Fragment {


    private List<Article> newsList = new ArrayList<Article>();
    private RecyclerView recyclerView;
    private BriefAdapter mAdapter;
    private ProgressBar my_progress;
    FrameLayout main_news;
    ImageView main_pic;
    TextView main_title;
    TextView link_to_api;

    public Briefs() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.brief_fragment, container, false);

        main_pic = view.findViewById(R.id.main_pic);
        main_title = view.findViewById(R.id.main_title);
        link_to_api = view.findViewById(R.id.link_to_api);
        main_news = view.findViewById(R.id.main_news);

        setUpSpannableTxt();

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        my_progress = view.findViewById(R.id.my_progress);
        mAdapter = new BriefAdapter(newsList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        prepareEgyNewsData();
        return view;
    }

    private void setUpSpannableTxt() {

        String powered_by = "Powerded by https://newsapi.org";
        SpannableString ss = new SpannableString(powered_by);
        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                //  Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://newsapi.org/"));
                //   startActivity(browserIntent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.BLUE);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan1, 11, powered_by.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        link_to_api.setText(ss);

      /*  //SpannableString ss = new SpannableString(powered_by);
        SpannableStringBuilder ssb = new SpannableStringBuilder(powered_by);
        ForegroundColorSpan fcsBlue = new ForegroundColorSpan(Color.BLUE);
        ssb.setSpan(fcsBlue, 11, powered_by.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        link_to_api.setText(ssb);
        */
        link_to_api.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://newsapi.org/"));
                startActivity(browserIntent);
            }
        });
    }

    private void prepareEgyNewsData() {
        Call<EgyptNews> call = ApiClient.
                getClient("https://newsapi.org/").
                getEgyptNews();

        call.enqueue(new Callback<EgyptNews>() {
            @Override
            public void onResponse(Call<EgyptNews> call, Response<EgyptNews> response) {

                if (!response.isSuccessful()) {

                    return;
                }
                my_progress.setVisibility(View.GONE);
                newsList = response.body().getArticles();
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();


                final Article article = response.body().getArticles().get(0);

                main_title.setText(article.getTitle());
                Picasso.get()
                        .load(article.getUrlToImage())
                        .into(main_pic, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {

                                //holder.progressBar.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError(Exception ex) {

                            }
                        });

                main_news.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), EgyNewsActivity.class);
                        intent.putExtra("title", article.getTitle());
                        intent.putExtra("author", String.valueOf(article.getAuthor()));
                        intent.putExtra("description", String.valueOf(article.getDescription()));
                        intent.putExtra("url", String.valueOf(article.getUrl()));
                        intent.putExtra("urlToImage", String.valueOf(article.getUrlToImage()));
                        intent.putExtra("publishedAt", String.valueOf(article.getPublishedAt()));
                        intent.putExtra("content", String.valueOf(article.getContent()));
                        intent.putExtra("source", String.valueOf(article.getSource().getName()));
                        getContext().startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<EgyptNews> call, Throwable t) {

            }


        });


    }
}
