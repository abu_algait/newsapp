package com.abualgait.newsapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class TopNews extends Fragment {

    private List<News> newsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private NewsAdapter mAdapter;

    public TopNews() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_news_fragment, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mAdapter = new NewsAdapter(newsList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        prepareMovieData();

        return view;
    }

    private void prepareMovieData() {

        News news = new News();
        news.setNews_id(1);
        news.setTitle("title1");
        news.setImage("https://images.unsplash.com/photo-1513836279014-a89f7a76ae86?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d4147a2e4e3f79299e2f0c92b13db9ee&w=1000&q=80");
        news.setDate("12 minutes ago");
        news.setComments("15 commments");

        News news1 = new News();
        news1.setNews_id(2);
        news1.setTitle("abck dksc");
        news1.setImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0ZLetF3wGrYTiytmeX0I5Nkuhtd8ewPv4W10aGZbMEcDUU822");
        news1.setDate("16 minutes ago");
        news1.setComments("161 commments");

        News news2 = new News();
        news2.setNews_id(3);
        news2.setTitle("qwer dasd");
        news2.setImage("https://assets.rbl.ms/6461854/980x.jpg");
        news2.setDate("19 minutes ago");
        news2.setComments("175 commments");


        News news3 = new News();
        news3.setNews_id(4);
        news3.setTitle("abc def");
        news3.setImage("https://assets.rbl.ms/6461854/980x.jpg");
        news3.setDate("19 minutes ago");
        news3.setComments("175 commments");

        newsList.add(news);
        newsList.add(news1);
        newsList.add(news2);
        newsList.add(news3);


        mAdapter.notifyDataSetChanged();
    }

}
