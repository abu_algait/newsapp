package com.abualgait.newsapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    private List<News> newsList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView news_item_image;
        private LinearLayout root_item_view;
        private ProgressBar progressBar;
        public TextView news_item_title, news_item_date, news_item_comment;

        public MyViewHolder(View view) {
            super(view);
            news_item_image = (ImageView) view.findViewById(R.id.news_item_image);
            news_item_title = (TextView) view.findViewById(R.id.news_item_title);
            news_item_date = (TextView) view.findViewById(R.id.news_item_date);
            news_item_comment = (TextView) view.findViewById(R.id.news_item_comment);
            root_item_view = (LinearLayout) view.findViewById(R.id.root_item_view);
            progressBar = (ProgressBar) view.findViewById(R.id.progress_bar_item);
        }
    }


    public NewsAdapter(List<News> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final News news = newsList.get(position);

        holder.news_item_title.setText(news.getTitle());
        holder.news_item_date.setText(news.getDate());
        holder.news_item_comment.setText(news.getComments());

        Picasso.get()
                .load(news.getImage())
                .into(holder.news_item_image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                });

        holder.root_item_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, Comments.class);
                intent.putExtra("image", news.getImage());
                intent.putExtra("title", news.getTitle());
                intent.putExtra("id", String.valueOf(news.getNews_id()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}