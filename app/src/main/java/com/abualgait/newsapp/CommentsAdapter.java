package com.abualgait.newsapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import models.Comment;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    private List<Comment> commentListc;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView owner_image;
        public TextView comment_time, comment_text, owner_name;

        public MyViewHolder(View view) {
            super(view);
            owner_image = (ImageView) view.findViewById(R.id.owner_image);
            comment_time = (TextView) view.findViewById(R.id.comment_time);
            comment_text = (TextView) view.findViewById(R.id.comment_text);
            owner_name = (TextView) view.findViewById(R.id.owner_name);

        }
    }


    public CommentsAdapter(List<Comment> commentListc, Context context) {
        this.commentListc = commentListc;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.comment_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Comment comment = commentListc.get(position);

        holder.comment_time.setText(comment.getComment_time());
        holder.comment_text.setText(comment.getCommnet_text());
        holder.owner_name.setText(comment.getOwner_email());

        Picasso.get()
                .load(comment.getOwner_image())
                .into(holder.owner_image);


    }

    @Override
    public int getItemCount() {
        return commentListc.size();
    }
}