package com.abualgait.newsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import utils.ConstantValues;
import utils.SharedPrefManger;

public class LoginActicity extends AppCompatActivity {

    private static final String TAG = "LoginActicity";

    @BindView(R.id.email_login)
    EditText email_login;

    @BindView(R.id.password_login)
    EditText password_login;


    @BindView(R.id.login_button)
    Button login_button;

    @BindView(R.id.signup_button)
    Button signup_button;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    FirebaseUser currentUser;
    private FirebaseAuth mAuth;

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        currentUser = mAuth.getCurrentUser();
        // updateUI(currentUser);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_acticity);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
    }

    public void clicked(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                login_method();
                break;
            case R.id.signup_button:
                signup_method();
                break;
        }

    }

    private void signup_method() {
        Intent i = new Intent(this, Register.class);
        startActivity(i);

    }

    private void login_method() {

        String email = email_login.getText().toString();
        String password = password_login.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            password_login.setError(getString(R.string.error_invalid_password));
            focusView = password_login;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            email_login.setError(getString(R.string.error_field_required));
            focusView = email_login;
            cancel = true;
        } else if (!isEmailValid(email)) {
            email_login.setError(getString(R.string.error_invalid_email));
            focusView = email_login;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            access_to_firebase();

        }
    }

    private void access_to_firebase() {
        progressBar.setVisibility(View.VISIBLE);
        String email;
        String password;// after Validation  make this
        email = email_login.getText().toString();
        password = password_login.getText().toString();

        final SharedPrefManger sharedPrefManager = new SharedPrefManger(this);
        final SharedPrefManger sharedPrefManager2 = new SharedPrefManger(getApplicationContext());
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //state success
                            String[] name = user.getEmail().split("@");
                            sharedPrefManager.setEditor(ConstantValues.IS_LOGGED_KEY, "true");
                            sharedPrefManager2.setEditor(ConstantValues.IS_LOGGED_KEY, "true");
                            sharedPrefManager.setEditor(ConstantValues.EMAIL_KEY, user.getEmail());
                            sharedPrefManager.setEditor(ConstantValues.NAME_KEY, name[0]);
                            sharedPrefManager.setEditor(ConstantValues.USER_KEY, user.getUid());
                            Intent intent = new Intent(LoginActicity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(), "some thing went wrong", Toast.LENGTH_SHORT).show();
                        }

                    }
                });


        // Toast.makeText(this, "some thing went wrong", Toast.LENGTH_SHORT).show();

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

}
