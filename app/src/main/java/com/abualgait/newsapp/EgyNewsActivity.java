package com.abualgait.newsapp;

import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EgyNewsActivity extends AppCompatActivity {
    @BindView(R.id.egy_news_image)
    ImageView egy_news_image;

    @BindView(R.id.createdat)
    TextView createdat;

    @BindView(R.id.news_title)
    TextView news_title;

    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.source)
    TextView source;


    @BindView(R.id.auther)
    TextView auther;

    @BindView(R.id.goandread)
    ConstraintLayout goandread;


    String title_str;
    String author_str;
    String description_str;
    String url_str;
    String urlToImage_str;
    String publishedAt_str;
    String content_str;
    String source_str;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_egy_news);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_black_24dp);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        title_str = intent.getStringExtra("title");
        author_str = intent.getStringExtra("author");
        description_str = intent.getStringExtra("description");
        url_str = intent.getStringExtra("url");
        urlToImage_str = intent.getStringExtra("urlToImage");
        publishedAt_str = intent.getStringExtra("publishedAt");
        content_str = intent.getStringExtra("content");
        source_str = intent.getStringExtra("source");
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>"+source_str + "</font>"));
        //getSupportActionBar().setTitle(author_str);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        intiviews();


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void intiviews() {
        Picasso.get()
                .load(urlToImage_str)
                .into(egy_news_image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        //holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                });

        news_title.setText(title_str);
        auther.setText(author_str);
        description.setText(description_str);
        createdat.setText(publishedAt_str);
        source.setText(source_str);
        goandread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url_str));
                startActivity(browserIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,title_str);
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, description_str);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
