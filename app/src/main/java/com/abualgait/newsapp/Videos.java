package com.abualgait.newsapp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import adapters.CommonAdapter;
import models.Video;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Videos extends Fragment {


    public Videos() {
    }

    private List<Video> videoList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommonAdapter mAdapter;
    private ProgressBar my_progress;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sports_fragment, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.videos_recycler_view);
        my_progress = view.findViewById(R.id.my_progress);
        mAdapter = new CommonAdapter(videoList, getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        preparePhotoData();
        return view;
    }

    private void preparePhotoData() {
        Call<List<Video>> call = ApiClient.getClient("https://api.jsonbin.io/").getVideos();

        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {

                if (!response.isSuccessful()) {

                    return;
                }
                my_progress.setVisibility(View.GONE);
                videoList = response.body();
                recyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {

            }


        });


    }

}
