package com.abualgait.newsapp;

import java.util.List;

import models.EgyptNews;
import models.Photo;
import models.Video;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterfacesFunctions {

    @GET("b/5bfef44c8d9b836588a11b9f")
    Call<List<Photo>> getPhotos();

    @GET("b/5c01b0dadf915b65399ab8b8")
    Call<List<Video>> getVideos();

    @GET("v2/top-headlines?country=eg&apiKey=f890dafcf7dc4bfc9d84e75c033abb8d")
    Call<EgyptNews> getEgyptNews();

}
