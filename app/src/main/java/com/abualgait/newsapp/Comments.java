package com.abualgait.newsapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import models.Comment;
import utils.ConstantValues;
import utils.SharedPrefManger;

public class Comments extends AppCompatActivity {

    @BindView(R.id.news_image)
    ImageView news_image;

    @BindView(R.id.news_title)
    TextView news_title;

    @BindView(R.id.news_comment)
    EditText news_comment;

    @BindView(R.id.add_comment)
    Button add_comment;

    @BindView(R.id.comments_recycler)
    RecyclerView comments_recycler;

    @BindView(R.id.progressBar3)
    ProgressBar progressBar3;


    SharedPrefManger sharedPrefManager = null;
    DatabaseReference myRef;
    FirebaseDatabase database;

    List<Comment> commentList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CommentsAdapter mAdapter;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_navigate_before_black_24dp);
        setSupportActionBar(toolbar);


        sharedPrefManager = new SharedPrefManger(this);

        // Write a message to the database
        database = FirebaseDatabase.getInstance();
        myRef = database.getReference("commnets");

        recyclerView = (RecyclerView) findViewById(R.id.comments_recycler);
        mAdapter = new CommentsAdapter(commentList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(false);


        Intent intent = getIntent();
        String image = intent.getStringExtra("image");
        String title = intent.getStringExtra("title");
        id = intent.getStringExtra("id");
        //getSupportActionBar().setTitle(title);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>"+title + "</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        news_title.setText(title);
        Picasso.get()
                .load(image)
                .into(news_image);
        read_comments();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public void add_comment(View view) {

        progressBar3.setVisibility(View.VISIBLE);
        String commnet_text = news_comment.getText().toString();
        String comment_time = get_current_time();
        String owner_email = sharedPrefManager.getEditor(ConstantValues.EMAIL_KEY);
        String owner_image = sharedPrefManager.getEditor(ConstantValues.IMAGE_KEY);
        Comment new_comment = new Comment(commnet_text, comment_time, owner_email, owner_image);
        String commentId = myRef.push().getKey();
        myRef.child(id).push().setValue(new_comment);
        read_comments();

    }

    private void read_comments() {
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentList.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String comment_id = postSnapshot.getKey();
                    if (comment_id.equals(id)) {

                        for (DataSnapshot comm : postSnapshot.getChildren()) {
                            Comment comment = comm.getValue(Comment.class);
                            commentList.add(comment);
                        }
                    }
                }
                mAdapter.notifyDataSetChanged();
                progressBar3.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("The read failed: " + error.getMessage());
            }
        });
    }


    private String get_current_time() {
        String timeStamp = new SimpleDateFormat("d MMM yyyy HH:mm aaa").format(Calendar.getInstance().getTime());
        return timeStamp;
    }
}
