package adapters;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abualgait.newsapp.Comments;
import com.abualgait.newsapp.EgyNewsActivity;
import com.abualgait.newsapp.News;
import com.abualgait.newsapp.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import models.Article;

public class BriefAdapter extends RecyclerView.Adapter<BriefAdapter.MyViewHolder> {

    private List<Article> newsList;
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView egy_news_image;
        public TextView source_tv, title_tv;
        ConstraintLayout main_const;

        public MyViewHolder(View view) {
            super(view);
            egy_news_image = (ImageView) view.findViewById(R.id.egy_news_image);
            source_tv = (TextView) view.findViewById(R.id.source_tv);
            title_tv = (TextView) view.findViewById(R.id.title_tv);
            main_const = (ConstraintLayout) view.findViewById(R.id.main_const);
        }
    }


    public BriefAdapter(List<Article> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.egy_news_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final Article news = newsList.get(position);
        holder.source_tv.setText(news.getSource().getName());
        holder.title_tv.setText(news.getTitle());
        Picasso.get()
                .load(news.getUrlToImage())
                .into(holder.egy_news_image, new com.squareup.picasso.Callback() {
                    @Override
                    public void onSuccess() {
                        //holder.progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception ex) {

                    }
                });

        holder.main_const.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, EgyNewsActivity.class);
                intent.putExtra("title", news.getTitle());
                intent.putExtra("author", String.valueOf(news.getAuthor()));
                intent.putExtra("description", String.valueOf(news.getDescription()));
                intent.putExtra("url", String.valueOf(news.getUrl()));
                intent.putExtra("urlToImage", String.valueOf(news.getUrlToImage()));
                intent.putExtra("publishedAt", String.valueOf(news.getPublishedAt()));
                intent.putExtra("content", String.valueOf(news.getContent()));
                intent.putExtra("source", String.valueOf(news.getSource().getName()));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return newsList.size();
    }
}