package adapters;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.squareup.picasso.Picasso;
import com.abualgait.newsapp.PhotoDetails;
import com.abualgait.newsapp.R;

import java.util.List;

import models.Article;
import models.Photo;
import models.Video;

public class CommonAdapter extends RecyclerView.Adapter<CommonAdapter.MyViewHolder> {

    private List<Photo> photoList;
    private List<Video> VideosList;
    private List<Article> articleList;
    Context context;
    int viewtype;
    private static int VIDEOS_VIEW_TYPE = 0;
    private static int PHOTOS_VIEW_TYPE = 1;
    private static int BRIEF_VIEW_TYPE = 2;

    public CommonAdapter(List<Photo> photoList, Context context, int viewtype) {
        this.photoList = photoList;
        this.context = context;
        this.viewtype = viewtype;
    }

    public CommonAdapter(List<Video> VideosList, Context context) {
        this.VideosList = VideosList;
        this.context = context;
        this.viewtype = VIDEOS_VIEW_TYPE;
    }

    public CommonAdapter(List<Article> articleLists, Context context, String egypt_news) {
        this.articleList = articleLists;
        this.viewtype = BRIEF_VIEW_TYPE;
        this.context = context;
    }

    // row elements
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView photo_item_image;
        private CardView root_item_view;
        private ProgressBar progressBar;
        private TextView photo_item_title;

        private VideoView mVideoView;
        private ImageView play;
        private ImageView pause;
        private TextView video_title;
        private ProgressBar buffer_progress_bar;
        private ImageSwitcher simpleImageSwitcher;

        public MyViewHolder(View view) {
            super(view);
            if (viewtype == PHOTOS_VIEW_TYPE) {
                photo_item_image = (ImageView) view.findViewById(R.id.photo_item_image);
                photo_item_title = (TextView) view.findViewById(R.id.photo_item_title);

                root_item_view = (CardView) view.findViewById(R.id.root_item_view);
                progressBar = (ProgressBar) view.findViewById(R.id.progress_bar_item);
            } else {
                buffer_progress_bar = view.findViewById(R.id.buffer_progress_bar);
                mVideoView = view.findViewById(R.id.video_view);
                play = view.findViewById(R.id.play);
                pause = view.findViewById(R.id.pause);
                video_title = view.findViewById(R.id.video_title);
                simpleImageSwitcher = (ImageSwitcher) view.findViewById(R.id.simpleImageSwitcher);
            }
        }
    }

    // row
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        if (viewType == PHOTOS_VIEW_TYPE) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.photo_item, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.video_item, parent, false);
        }
        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        if (viewtype == PHOTOS_VIEW_TYPE) {
            final Photo photo = photoList.get(position);

            // Set the Data in the Row
            holder.photo_item_title.setText(photo.getTitle());
            Picasso.get()
                    .load(photo.getImage())
                    .into(holder.photo_item_image, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception ex) {
                        }
                    });

            holder.root_item_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Toast.makeText(context, String.valueOf(position), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, PhotoDetails.class);
                    intent.putExtra("image", photo.getImage());
                    intent.putExtra("title", photo.getTitle());
                    context.startActivity(intent);
                }
            });
        } else {
            final Video video = VideosList.get(position);

            holder.video_title.setText(video.getTitle());

            //String videoUrl = "http://www.quirksmode.org/html5/videos/big_buck_bunny.mp4";
            // Set progressbar message
            holder.buffer_progress_bar.setVisibility(View.VISIBLE);

            try {
                // Start the MediaController
                MediaController mediacontroller = new MediaController(context);
                mediacontroller.setAnchorView(holder.mVideoView);
                Uri videoUri = Uri.parse(video.getUrl());
                holder.mVideoView.setMediaController(mediacontroller);
                holder.mVideoView.setVideoURI(videoUri);

            } catch (Exception e) {

                e.printStackTrace();
            }
            holder.play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.simpleImageSwitcher.setDisplayedChild(1);
                    holder.mVideoView.start();
                }
            });

            holder.pause.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    holder.simpleImageSwitcher.setDisplayedChild(0);
                    holder.mVideoView.pause();
                }
            });

            holder.mVideoView.requestFocus();
            holder.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                // Close the progress bar and play the video
                public void onPrepared(MediaPlayer mp) {
                    holder.buffer_progress_bar.setVisibility(View.GONE);
                    mp.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        @Override
                        public boolean onInfo(MediaPlayer mp, int what, int extra) {
                            if (what == MediaPlayer.MEDIA_INFO_BUFFERING_START)
                                holder.buffer_progress_bar.setVisibility(View.VISIBLE);
                            if (what == MediaPlayer.MEDIA_INFO_BUFFERING_END)
                                holder.buffer_progress_bar.setVisibility(View.GONE);
                            return false;
                        }
                    });


                }
            });

            holder.mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                public void onCompletion(MediaPlayer mp) {
                    holder.buffer_progress_bar.setVisibility(View.GONE);

                }
            });
        }

    }

    @Override
    public int getItemCount() {
        if (viewtype == 1) {
            return photoList.size();
        } else {
            return VideosList.size();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (viewtype == 1)
            return PHOTOS_VIEW_TYPE;
        else
            return VIDEOS_VIEW_TYPE;

    }
}