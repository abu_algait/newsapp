// Generated code from Butter Knife. Do not modify!
package com.abualgait.newsapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.constraint.ConstraintLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EgyNewsActivity_ViewBinding implements Unbinder {
  private EgyNewsActivity target;

  @UiThread
  public EgyNewsActivity_ViewBinding(EgyNewsActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public EgyNewsActivity_ViewBinding(EgyNewsActivity target, View source) {
    this.target = target;

    target.egy_news_image = Utils.findRequiredViewAsType(source, R.id.egy_news_image, "field 'egy_news_image'", ImageView.class);
    target.createdat = Utils.findRequiredViewAsType(source, R.id.createdat, "field 'createdat'", TextView.class);
    target.news_title = Utils.findRequiredViewAsType(source, R.id.news_title, "field 'news_title'", TextView.class);
    target.description = Utils.findRequiredViewAsType(source, R.id.description, "field 'description'", TextView.class);
    target.source = Utils.findRequiredViewAsType(source, R.id.source, "field 'source'", TextView.class);
    target.auther = Utils.findRequiredViewAsType(source, R.id.auther, "field 'auther'", TextView.class);
    target.goandread = Utils.findRequiredViewAsType(source, R.id.goandread, "field 'goandread'", ConstraintLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EgyNewsActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.egy_news_image = null;
    target.createdat = null;
    target.news_title = null;
    target.description = null;
    target.source = null;
    target.auther = null;
    target.goandread = null;
  }
}
