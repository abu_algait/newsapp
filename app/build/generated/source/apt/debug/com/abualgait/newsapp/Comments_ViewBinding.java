// Generated code from Butter Knife. Do not modify!
package com.abualgait.newsapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Comments_ViewBinding implements Unbinder {
  private Comments target;

  @UiThread
  public Comments_ViewBinding(Comments target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Comments_ViewBinding(Comments target, View source) {
    this.target = target;

    target.news_image = Utils.findRequiredViewAsType(source, R.id.news_image, "field 'news_image'", ImageView.class);
    target.news_title = Utils.findRequiredViewAsType(source, R.id.news_title, "field 'news_title'", TextView.class);
    target.news_comment = Utils.findRequiredViewAsType(source, R.id.news_comment, "field 'news_comment'", EditText.class);
    target.add_comment = Utils.findRequiredViewAsType(source, R.id.add_comment, "field 'add_comment'", Button.class);
    target.comments_recycler = Utils.findRequiredViewAsType(source, R.id.comments_recycler, "field 'comments_recycler'", RecyclerView.class);
    target.progressBar3 = Utils.findRequiredViewAsType(source, R.id.progressBar3, "field 'progressBar3'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Comments target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.news_image = null;
    target.news_title = null;
    target.news_comment = null;
    target.add_comment = null;
    target.comments_recycler = null;
    target.progressBar3 = null;
  }
}
