// Generated code from Butter Knife. Do not modify!
package com.abualgait.newsapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LoginActicity_ViewBinding implements Unbinder {
  private LoginActicity target;

  @UiThread
  public LoginActicity_ViewBinding(LoginActicity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LoginActicity_ViewBinding(LoginActicity target, View source) {
    this.target = target;

    target.email_login = Utils.findRequiredViewAsType(source, R.id.email_login, "field 'email_login'", EditText.class);
    target.password_login = Utils.findRequiredViewAsType(source, R.id.password_login, "field 'password_login'", EditText.class);
    target.login_button = Utils.findRequiredViewAsType(source, R.id.login_button, "field 'login_button'", Button.class);
    target.signup_button = Utils.findRequiredViewAsType(source, R.id.signup_button, "field 'signup_button'", Button.class);
    target.progressBar = Utils.findRequiredViewAsType(source, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    LoginActicity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.email_login = null;
    target.password_login = null;
    target.login_button = null;
    target.signup_button = null;
    target.progressBar = null;
  }
}
