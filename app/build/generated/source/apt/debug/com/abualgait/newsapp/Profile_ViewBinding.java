// Generated code from Butter Knife. Do not modify!
package com.abualgait.newsapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import de.hdodenhof.circleimageview.CircleImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Profile_ViewBinding implements Unbinder {
  private Profile target;

  @UiThread
  public Profile_ViewBinding(Profile target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Profile_ViewBinding(Profile target, View source) {
    this.target = target;

    target.user_name = Utils.findRequiredViewAsType(source, R.id.user_name, "field 'user_name'", TextView.class);
    target.user_email = Utils.findRequiredViewAsType(source, R.id.user_email, "field 'user_email'", TextView.class);
    target.profile_image = Utils.findRequiredViewAsType(source, R.id.profile_image, "field 'profile_image'", CircleImageView.class);
    target.floatingActionButton = Utils.findRequiredViewAsType(source, R.id.floating, "field 'floatingActionButton'", FloatingActionButton.class);
    target.notification_recycler = Utils.findRequiredViewAsType(source, R.id.notification_recycler, "field 'notification_recycler'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Profile target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.user_name = null;
    target.user_email = null;
    target.profile_image = null;
    target.floatingActionButton = null;
    target.notification_recycler = null;
  }
}
