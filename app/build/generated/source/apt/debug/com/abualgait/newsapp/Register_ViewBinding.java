// Generated code from Butter Knife. Do not modify!
package com.abualgait.newsapp;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Register_ViewBinding implements Unbinder {
  private Register target;

  @UiThread
  public Register_ViewBinding(Register target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Register_ViewBinding(Register target, View source) {
    this.target = target;

    target.username = Utils.findRequiredViewAsType(source, R.id.username, "field 'username'", EditText.class);
    target.email_register_et = Utils.findRequiredViewAsType(source, R.id.email_register, "field 'email_register_et'", EditText.class);
    target.password_register_et = Utils.findRequiredViewAsType(source, R.id.password, "field 'password_register_et'", EditText.class);
    target.register_btn = Utils.findRequiredViewAsType(source, R.id.register, "field 'register_btn'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Register target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.username = null;
    target.email_register_et = null;
    target.password_register_et = null;
    target.register_btn = null;
  }
}
